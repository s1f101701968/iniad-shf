# Generated by Django 2.2.6 on 2019-10-23 05:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Hotels', '0003_auto_20191023_1243'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='condition',
        ),
        migrations.AddField(
            model_name='item',
            name='condition',
            field=models.ManyToManyField(blank=True, choices=[(1, 'ショッピング'), (2, '娯楽施設'), (3, '温泉'), (4, '日本文化体験'), (5, '食の名産'), (6, 'テーマパーク'), (7, '景色')], default=1, null=True, related_name='_item_condition_+', to='Hotels.Item', verbose_name='条件'),
        ),
    ]
