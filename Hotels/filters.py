from django_filters import filters
from django_filters import FilterSet
from .models import Item


class MyOrderingFilter(filters.OrderingFilter):
    descending_fmt = '%s （降順）'


class ItemFilter(FilterSet):

    stadium = filters.CharFilter(label='会場', lookup_expr='contains')
    fee = filters.ChoiceFilter(choices=Item.FEE_CHOICES, label="料金", lookup_expr='contains')
    condition = filters.MultipleChoiceFilter(choices=Item.CONDITION_CHOICES, label="条件", lookup_expr='contains')

    order_by = MyOrderingFilter(
        # tuple-mapping retains order
        fields=(
            ('fee_choices', 'fee_choices')
        ),
        field_labels={
            'fee': '料金'
        },
        label='並び順'
    )

    class Meta:

        model = Item
        fields = ('stadium', 'fee_choices', 'condition',)
