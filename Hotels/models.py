from django.db import models


class Item(models.Model):
    CONDITION_CHOICES = (
        (1, "ショッピング"),
        (2, "娯楽施設"),
        (3, "温泉"),
        (4, "日本文化体験"),
        (5, "食の名産"),
        (6, "テーマパーク"),
        (7, "景色")
    )
    FEE_CHOICES = (
        (1, "~1万円"),
        (2, "~2万円"),
        (3, "~3万円")
    )
    name = models.CharField(
        verbose_name='宿名',
        max_length=50,
        blank=True,
        null=True
    )
    stadium = models.CharField(
        verbose_name='会場',
        max_length=40,
        blank=True,
        null=True
    )
    condition = models.IntegerField(
        verbose_name='条件',
        choices=CONDITION_CHOICES,
        default=1,
        blank=True,
        null=True,
    )
    fee = models.IntegerField(
        verbose_name="料金",
        blank=True,
        null=True
    )
    fee_choices = models.IntegerField(
        verbose_name='料金幅',
        choices=FEE_CHOICES,
        default=1,
        blank=True,
        null=True,
    )
    area = models.CharField(
        verbose_name='エリア',
        max_length=20,
        blank=True,
        null=True
    )
    image = models.ImageField(
        verbose_name="写真",
        upload_to='media',
        blank=True,
        null=True)
    detail = models.TextField(
        verbose_name='詳細',
        blank=True,
        null=True)
    created_at = models.DateTimeField(
        verbose_name='登録日',
        auto_now_add=True
    )

    # 以下は管理サイト上の表示設定
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'アイテム'
        verbose_name_plural = 'アイテム'
