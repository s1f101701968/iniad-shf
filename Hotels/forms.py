from django import forms
from .models import Item


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ('stadium', 'fee', 'condition')
        widgets = {
            'stadium': forms.TextInput(attrs={'placeholder': '記入例：Olympic Stadium'}),
            'fee': forms.SelectMultiple(choices=Item.FEE_CHOICES),
            'condition': forms.SelectMultiple(choices=Item.CONDITION_CHOICES)
        }
